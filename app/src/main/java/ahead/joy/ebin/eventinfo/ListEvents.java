package ahead.joy.ebin.eventinfo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ahead.joy.ebin.eventinfo.adapters.EventListAdapter;
import ahead.joy.ebin.eventinfo.model.Event;
import retrofit.ApiClient;
import retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import show.event.EventShowActivity;

public class ListEvents extends AppCompatActivity implements EventListAdapter.ClickListener{

    ProgressBar pb;
    TextView tv_failded;
    AppCompatButton button_retry;
    RecyclerView rv;
    List<Event> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_events);
        rv = (RecyclerView) findViewById(R.id.rv_list_events);
        pb = (ProgressBar) findViewById(R.id.progressBar2);
        tv_failded = (TextView) findViewById(R.id.tv_failed);
        button_retry = (AppCompatButton) findViewById(R.id.btn_retry);
        button_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUPRecyclerView();
            }
        });
        setUPRecyclerView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUPRecyclerView();
    }

    private void setUPRecyclerView() {
        pb.setVisibility(View.VISIBLE);
        rv.setVisibility(View.GONE);
        tv_failded.setVisibility(View.GONE);
        button_retry.setVisibility(View.GONE);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<List<Event>> call = apiService.getEvents();
        call.enqueue(new Callback<List<Event>>() {

            @Override
            public void onResponse(Call<List<Event>> call, retrofit2.Response<List<Event>> response) {

//                Log.e("2222", "Number of events received: " + );
                if (response.body().size() == 0) {
                    pb.setVisibility(View.GONE);
                    rv.setVisibility(View.GONE);
                    tv_failded.setVisibility(View.VISIBLE);
                    button_retry.setVisibility(View.GONE);
                    tv_failded.setText("No events found");
                } else {
                    try {
                        events = response.body();
                        EventListAdapter mAdapter = new EventListAdapter(events, ListEvents.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ListEvents.this);
                        rv.setLayoutManager(mLayoutManager);
                        rv.setItemAnimator(new DefaultItemAnimator());
                        rv.setAdapter(mAdapter);
                mAdapter.setClickListener(ListEvents.this);
                        pb.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        tv_failded.setVisibility(View.GONE);
                        button_retry.setVisibility(View.GONE);
                    } catch (Exception exc) {
                        Log.e("Error while parse", exc.toString());
                        pb.setVisibility(View.GONE);
                        rv.setVisibility(View.GONE);
                        tv_failded.setVisibility(View.VISIBLE);
                        button_retry.setVisibility(View.VISIBLE);
                        tv_failded.setText("Server response error");

                    }
                }
            }
            @Override
            public void onFailure(Call<List<Event>>call, Throwable t) {
                // Log error here since request failed
                pb.setVisibility(View.GONE);
                rv.setVisibility(View.GONE);
                tv_failded.setVisibility(View.VISIBLE);
                button_retry.setVisibility(View.VISIBLE);
                tv_failded.setText(getResources().getString(R.string.canotConnect));
            }
        });
    }

    @Override
    public void checkInListClick(int position) {
        if(events!=null){
            Intent in = new Intent(ListEvents.this, EventShowActivity.class);
            Bundle b = new Bundle();
            b.putInt("id",events.get(position).getId());
            in.putExtras(b);
            startActivity(in);
        }
    }
}
