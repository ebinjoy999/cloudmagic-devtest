package show.event;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import ahead.joy.ebin.eventinfo.DepricationCompact;
import ahead.joy.ebin.eventinfo.ListEvents;
import ahead.joy.ebin.eventinfo.R;
import ahead.joy.ebin.eventinfo.adapters.EventListAdapter;
import ahead.joy.ebin.eventinfo.model.Event;
import retrofit.ApiClient;
import retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import show.event.model.EventDetail;
import show.event.model.Participant;

public class EventShowActivity extends AppCompatActivity {

    int id;
    MaterialDialog progressD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_show);

        progressD =  new MaterialDialog.Builder(this).backgroundColor(DepricationCompact.getColor(this,R.color.colorPrimary))
                .contentColor(DepricationCompact.getColor(this,R.color.white))
                .content("Loading")
                .progress(true, 0).autoDismiss(false).build();

        id = getIntent().getExtras().getInt("id");
        Log.e("tag",id+"");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpFields();

        ((AppCompatButton) findViewById(R.id.btn_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressD.show();
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);

                Call<EventDetail> call = apiService.getDelete(id);
                call.enqueue(new Callback<EventDetail>() {

                    @Override
                    public void onResponse(Call<EventDetail> call, retrofit2.Response<EventDetail> response) {
//                    Log.e("tag","sds");
                        progressD.dismiss();
                        EventShowActivity.this.finish();
                    }
                    @Override
                    public void onFailure(Call<EventDetail>call, Throwable t) {
                        // Log error here since request failed
//                        Log.e("tag",t.toString());
                        progressD.dismiss();
                        Toast.makeText(EventShowActivity.this,"Something went wrong!",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    private void setUpFields() {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<EventDetail> call = apiService.getEventDetail(id);
        call.enqueue(new Callback<EventDetail>() {

            @Override
            public void onResponse(Call<EventDetail> call, retrofit2.Response<EventDetail> response) {
//                    Log.e("tag",response.body().getSubject());
                ((TextView) findViewById(R.id.textView_subject)).setText(response.body().getSubject());
                ((TextView) findViewById(R.id.textView_preview)).setText(response.body().getPreview());
                ((TextView) findViewById(R.id.textView_body)).setText(response.body().getBody());
                if(response.body().getIsStarred())
                    ((TextView) findViewById(R.id.textView_satrted)).setText("Started");
                    else
                    ((TextView) findViewById(R.id.textView_satrted)).setText("Idle");

                     String participant = "";
                    for(Participant p : response.body().getParticipants()){
                        participant = participant + "\n"+ p.getName()+"\n"+ p.getEmail();
                    }
                ((TextView) findViewById(R.id.textView_participants)).setText(participant);
            }
            @Override
            public void onFailure(Call<EventDetail>call, Throwable t) {
                // Log error here since request failed
                Log.e("tag",t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
