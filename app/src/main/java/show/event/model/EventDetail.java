package show.event.model;


import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventDetail {

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("participants")
    @Expose
    private List<Participant> participants = new ArrayList<Participant>();
    @SerializedName("preview")
    @Expose
    private String preview;
    @SerializedName("isRead")
    @Expose
    private Boolean isRead;
    @SerializedName("isStarred")
    @Expose
    private Boolean isStarred;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("ts")
    @Expose
    private Integer ts;

    /**
     * No args constructor for use in serialization
     *
     */
    public EventDetail() {
    }

    /**
     *
     * @param id
     * @param body
     * @param ts
     * @param preview
     * @param subject
     * @param isStarred
     * @param participants
     * @param isRead
     */
    public EventDetail(String subject, List<Participant> participants, String preview, Boolean isRead, Boolean isStarred, Integer id, String body, Integer ts) {
        this.subject = subject;
        this.participants = participants;
        this.preview = preview;
        this.isRead = isRead;
        this.isStarred = isStarred;
        this.id = id;
        this.body = body;
        this.ts = ts;
    }

    /**
     *
     * @return
     * The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     *
     * @param subject
     * The subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     *
     * @return
     * The participants
     */
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     *
     * @param participants
     * The participants
     */
    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    /**
     *
     * @return
     * The preview
     */
    public String getPreview() {
        return preview;
    }

    /**
     *
     * @param preview
     * The preview
     */
    public void setPreview(String preview) {
        this.preview = preview;
    }

    /**
     *
     * @return
     * The isRead
     */
    public Boolean getIsRead() {
        return isRead;
    }

    /**
     *
     * @param isRead
     * The isRead
     */
    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    /**
     *
     * @return
     * The isStarred
     */
    public Boolean getIsStarred() {
        return isStarred;
    }

    /**
     *
     * @param isStarred
     * The isStarred
     */
    public void setIsStarred(Boolean isStarred) {
        this.isStarred = isStarred;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The body
     */
    public String getBody() {
        return body;
    }

    /**
     *
     * @param body
     * The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     *
     * @return
     * The ts
     */
    public Integer getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(Integer ts) {
        this.ts = ts;
    }

}