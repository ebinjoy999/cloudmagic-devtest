package ahead.joy.ebin.eventinfo.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import ahead.joy.ebin.eventinfo.DepricationCompact;
import ahead.joy.ebin.eventinfo.R;
import ahead.joy.ebin.eventinfo.model.Event;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder> {

    private List<Event> moviesList;
    ClickListener clkL;
    Context c;
    boolean first_time = true;
    int a;

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        public TextView tv_subject, tv_started, tv_preview;
        ImageView iv_first_letter;
        public MyViewHolder(View view) {
            super(view);
            tv_subject = (TextView) view.findViewById(R.id.tv_subject);
            iv_first_letter = (ImageView) view.findViewById(R.id.iv_first_letter);
            tv_started = (TextView) view.findViewById(R.id.tv_started);
            tv_preview = (TextView) view.findViewById(R.id.tv_preview) ;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(clkL!=null)clkL.checkInListClick(getAdapterPosition());
        }
    }


    public EventListAdapter(List<Event> moviesList, Context c) {
        this.moviesList = moviesList;
        this.c = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_event_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (first_time) {
            a = (int) (holder.iv_first_letter.getLayoutParams().height * .4);
            first_time = false;
        }
        Event movie = moviesList.get(position);
        holder.tv_subject.setText(movie.getSubject());
        holder.tv_preview.setText(movie.getPreview());
        if (movie.getIsStarred()) holder.tv_started.setText("Started");
        else holder.tv_started.setText("Idle");

        int color = R.color.colorPrimary;
        int random = position % 3;
        if (random == 2) {
            color = R.color.color1;
        }
        if (random == 1) {
            color = R.color.color2;
        }
        if (random == 0) {
            color = R.color.color3;
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig().fontSize(a).endConfig()
                .buildRound(movie.getSubject().charAt(0)+"", DepricationCompact.getColor(c, color));

        holder.iv_first_letter.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    public  void setClickListener(ClickListener clkL){
        this.clkL = clkL;
    }
    public interface ClickListener{
        public void checkInListClick(int position);
    }
}