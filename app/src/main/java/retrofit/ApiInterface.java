package retrofit;


import java.util.List;

import ahead.joy.ebin.eventinfo.model.Event;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;
import show.event.model.EventDetail;

public interface ApiInterface {

    //delete a specific event
//    @GET("checkins/{id}")
//    Call<Checkin> getSingleCheckin(@Header("Authorization") String token,
//                                   @Header("Content-Type") String token2, @Path("id") int id);

    @GET("message")
    Call<List<Event>> getEvents();

    @GET("message/{id}")
    Call<EventDetail> getEventDetail(@Path("id") int id);

    @DELETE("message/{id}")
    Call<EventDetail> getDelete(@Path("id") int id);

}